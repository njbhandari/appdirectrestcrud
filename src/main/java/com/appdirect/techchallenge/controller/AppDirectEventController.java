package com.appdirect.techchallenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.techchallenge.service.CustomerService;

@RestController
public class AppDirectEventController extends BaseController {

	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/subscription")
	public ResponseEntity<String> getCustomers(@RequestParam("eventUrl") String eventUrl) {
		try{
			customerService.createCustFromAppDirect(eventUrl);
		}catch (Exception e){
			e.printStackTrace();
			handleException(e, e.getMessage());
		}
		return new ResponseEntity<String>("get event called "+eventUrl, HttpStatus.ACCEPTED);
	}

}
