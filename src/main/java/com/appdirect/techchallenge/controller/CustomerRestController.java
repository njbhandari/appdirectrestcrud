package com.appdirect.techchallenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.techchallenge.model.Customer;
import com.appdirect.techchallenge.service.CustomerService;

@RestController
public class CustomerRestController extends BaseController {

	
	@Autowired
	private CustomerService customerService;

	
	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> getCustomers() {
		List<Customer> customers = null;
		
		try{
			customers =customerService.list();
		}catch(Exception e) {
			handleException(e, "error while fetching customer list");
		}
		
		return new ResponseEntity<List<Customer>> (customers, HTTP_STATUS_OK);
	}

	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable("id") String id) {

		Customer customer  = null;
		try{
			customer = customerService.get(id);
			
		}catch(Exception e) {
			handleException(e, "error fetching customer details for customer with id "+ id);
		}

		return new ResponseEntity<Customer>(customer, HTTP_STATUS_OK);
	}

	@PostMapping(value = "/customer")
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		try{
			customerService.create(customer);
		}catch(Exception e){
			handleException(e, "Error Creating customer");
		}

		return new ResponseEntity<Customer>(customer, HTTP_STATUS_OK);
	}

	@DeleteMapping("/customer/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable String id) {
		try{
			customerService.delete(id);		
		}catch(Exception e) {
			handleException(e,"Error While Deleting Customer");
		}

		return new ResponseEntity<String>("Customer with id "+id+" deleted", HTTP_STATUS_OK);

	}

	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable String id, @RequestBody Customer customer) {
		try{
			customer = customerService.update(id, customer);			
		}catch(Exception e) {
			handleException(e, "Error Updating Customer deatils for customer with ID "+id);
		}

		return new ResponseEntity<Customer>(customer, HTTP_STATUS_OK);
	}

}