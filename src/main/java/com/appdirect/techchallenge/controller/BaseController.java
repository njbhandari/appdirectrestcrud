package com.appdirect.techchallenge.controller;

import org.springframework.http.HttpStatus;

import com.appdirect.techchallenge.exception.AppDirectException;

public abstract class BaseController {

	protected static final HttpStatus HTTP_STATUS_OK = HttpStatus.OK;
	protected static final Integer HTTP_STATU_NOT_FOUND = HttpStatus.NOT_FOUND.value();
	
	protected void handleException(Exception e, String message) {
		AppDirectException appDirectException = null;
		
		if(e instanceof Exception) {
			appDirectException = new AppDirectException(e.getMessage(), message , HttpStatus.INTERNAL_SERVER_ERROR.value());			
		} else if(e instanceof AppDirectException){
			appDirectException = (AppDirectException) e;
		}
		throw appDirectException;
	}
	
}
