package com.appdirect.techchallenge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appdirect.techchallenge.dao.CustomerDAO;
import com.appdirect.techchallenge.exception.AppDirectException;
import com.appdirect.techchallenge.model.Customer;

import static com.appdirect.techchallenge.literals.AppDirectLiterals.HTTP_STATUS_NOT_FOUND;
import static com.appdirect.techchallenge.literals.AppDirectLiterals.CUSTOMER_NOT_FOUND;;


@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;
	
	public List<Customer> list() throws Exception {
		
		return customerDAO.list();
		
	}

	public Customer get(String id) throws Exception {
		Customer customer = customerDAO.get(id);
		if (customer == null) {
			throw new AppDirectException(CUSTOMER_NOT_FOUND+ id, CUSTOMER_NOT_FOUND+ id, HTTP_STATUS_NOT_FOUND);
		}
		return customer;
	}

	public Customer create(Customer customer) throws Exception {
		
		return customerDAO.create(customer);
	}

	public String delete(String id) throws Exception {
		String deleteCustomerId = customerDAO.delete(id);
		
		if (null == deleteCustomerId) {
			throw new AppDirectException(CUSTOMER_NOT_FOUND+ id, CUSTOMER_NOT_FOUND+ id, HTTP_STATUS_NOT_FOUND);
		}
		
		return deleteCustomerId;
	}

	public Customer update(String id, Customer customer) throws Exception {
		Customer updatedCustomer = customerDAO.update(id, customer);
		
		if (null == updatedCustomer) {
			throw new AppDirectException(CUSTOMER_NOT_FOUND+ id, CUSTOMER_NOT_FOUND+ id, HTTP_STATUS_NOT_FOUND);
		}
		
		return updatedCustomer;
	}

	public String createCustFromAppDirect(String eventUrl) throws Exception {

		//TODO using event url call the event url and get the user details and add it to customer object below.
		Customer newCustomer = new Customer();
		newCustomer.setId(eventUrl);
		newCustomer.setFirstName("AppDirectUser");
		newCustomer.setLastName("AppDirectUser");
		customerDAO.create(newCustomer);
		return "Success";
	}

}
