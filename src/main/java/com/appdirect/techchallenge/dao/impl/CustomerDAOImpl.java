package com.appdirect.techchallenge.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.appdirect.techchallenge.dao.CustomerDAO;
import com.appdirect.techchallenge.model.Customer;

@Component
public class CustomerDAOImpl implements CustomerDAO {

	// Dummy database. Initialize with some dummy values.
	private static List<Customer> customers;
	{
		customers = new ArrayList<Customer>();
		customers.add(new Customer("101", "Nikhil", "Bhandari", "nikhiljbhandari@gmail.com", "121-232-3435"));
		customers.add(new Customer("201", "Aayushi", "Bhandari", "anbhandari@gmail.com", "343-545-2345"));
		customers.add(new Customer("301", "Vaishnavi", "bhandari", "vbhandari@gmail.com", "876-237-2987"));
	}

	/**
	 * Returns list of customers from dummy database.
	 * 
	 * @return list of customers
	 */
	public List<Customer> list() throws Exception {
		return customers;
	}

	/**
	 * Return customer object for given id from dummy database. If customer is
	 * not found for id, returns null.
	 * 
	 * @param id
	 *            customer id
	 * @return customer object for given id
	 */
	public Customer get(String id) throws Exception {

		for (Customer c : customers) {
			if (c.getId().equalsIgnoreCase(id)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * Create new customer in dummy database. Updates the id and insert new
	 * customer in list.
	 * 
	 * @param customer
	 *            Customer object
	 * @return customer object with updated id
	 */
	public Customer create(Customer customer) throws Exception {
		customer.setId(String.valueOf(System.currentTimeMillis()));
		customers.add(customer);
		return customer;
	}

	/**
	 * Delete the customer object from dummy database. If customer not found for
	 * given id, returns null.
	 * 
	 * @param id
	 *            the customer id
	 * @return id of deleted customer object
	 */
	public String delete(String id) throws Exception{

		for (Customer c : customers) {
			if (c.getId().equalsIgnoreCase(id)) {
				customers.remove(c);
				return id;
			}
		}

		return null;
	}

	/**
	 * Update the customer object for given id in dummy database. If customer
	 * not exists, returns null
	 * 
	 * @param id
	 * @param customer
	 * @return customer object with id
	 */
	public Customer update(String id, Customer customer)throws Exception {

		for (Customer c : customers) {
			if (c.getId().equalsIgnoreCase(id)) {
				customer.setId(c.getId());
				customers.remove(c);
				customers.add(customer);
				return customer;
			}
		}

		return null;
	}

}