package com.appdirect.techchallenge.dao;

import java.util.List;

import com.appdirect.techchallenge.model.Customer;

public interface CustomerDAO {

	public List<Customer> list() throws Exception;
	public Customer get(String id) throws Exception;
	public Customer create(Customer customer) throws Exception;
	public String delete(String id) throws Exception;
	public Customer update(String id, Customer customer) throws Exception;
	
}
