package com.appdirect.techchallenge.model;

public class ErrorDTO {
	
	private String errorID;
	private String errorMessage;
	private String userMessage;
	
	public String getErrorID() {
		return errorID;
	}
	public void setErrorID(String errorID) {
		this.errorID = errorID;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getUserMessage() {
		return userMessage;
	}
	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
	
	

}
