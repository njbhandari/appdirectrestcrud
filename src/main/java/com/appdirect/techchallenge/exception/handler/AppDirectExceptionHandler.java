package com.appdirect.techchallenge.exception.handler;

import java.util.UUID;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.appdirect.techchallenge.exception.AppDirectException;
import com.appdirect.techchallenge.model.ErrorDTO;

@ControllerAdvice
public class AppDirectExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(value = {AppDirectException.class})
	protected ResponseEntity<Object> handleException(AppDirectException exception, WebRequest request){
		
		ErrorDTO  errorDTO= new ErrorDTO();
		errorDTO.setErrorID(UUID.randomUUID().toString());
		errorDTO.setErrorMessage(exception.getMessage());
		errorDTO.setUserMessage(exception.getUserMessage());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return handleExceptionInternal(exception, errorDTO, headers,
				HttpStatus.valueOf(exception.getCode()),
				request);
	}
	
}
