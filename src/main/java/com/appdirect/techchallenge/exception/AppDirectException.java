package com.appdirect.techchallenge.exception;

public class AppDirectException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userMessage;
	private Integer code=500;
	
	public AppDirectException(String userMessage, String errorMessage,
			Integer code) {
		super(errorMessage);
		this.userMessage = userMessage;
		this.code = code;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	

}
