package com.appdirect.techchallenge.literals;

import org.springframework.http.HttpStatus;

public class AppDirectLiterals {
	
	public static final Integer HTTP_STATUS_NOT_FOUND = HttpStatus.NOT_FOUND.value();
	public static final String CUSTOMER_NOT_FOUND = "No Customer found for ID ";
	
}
